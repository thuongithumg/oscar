<?php

namespace Oscar\Faq\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\DB\Ddl\Table;
class UpgradeSchema implements UpgradeSchemaInterface
{

    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;

        $installer->startSetup();

        //xóa columns
        if (version_compare($context->getVersion(), '1.0.1', '<')) {

            $installer->getConnection()->dropColumn(
                $installer->getTable('faq_category'),
                'ordering'
            );
            $installer->getConnection()->dropColumn(
                $installer->getTable('faq'),
                'ordering'
            );
        }
        //add columns
        if (version_compare($context->getVersion(), '1.0.2' , '<')) {

            // Get module table
            $tableName1 = $setup->getTable('faq_category');
            $tableName2 = $setup->getTable('faq');

            // Check if the table already exists
            if ($setup->getConnection()->isTableExists($tableName1) == true) {
                // Declare data
                $columns = [
                    'sort_order' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'size'=> null,
                        'operation'=>['nullable' => false,'unsigned' => true,],
                        'comment' => 'sort_order',
                    ],
                ];

                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName1, $name, $definition);
                }
            }
            if ($setup->getConnection()->isTableExists($tableName2) == true) {
                // Declare data
                $columns = [
                    'sort_order' => [
                        'type' => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
                        'size'=> null,
                        'operation'=>['nullable' => false,'unsigned' => true,],
                        'comment' => 'sort_order',
                    ],
                ];
                $connection = $setup->getConnection();
                foreach ($columns as $name => $definition) {
                    $connection->addColumn($tableName2, $name, $definition);
                }
            }
        }
        $setup->endSetup();
    }
}